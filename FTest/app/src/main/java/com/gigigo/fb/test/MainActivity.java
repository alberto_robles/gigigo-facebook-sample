package com.gigigo.fb.test;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;


public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG="GigigoFacebookDemo";
    private CallbackManager callbackManager;
    private AccessToken accessToken;
    private User demoUser;

    private EditText textName;
    private EditText textMail;
    private EditText textBirthDate;
    private RadioGroup radioGroupGender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textName = (EditText) findViewById(R.id.editText_name);
        textMail= (EditText) findViewById(R.id.editText_mail);
        textBirthDate= (EditText) findViewById(R.id.editText_birthdate);
        radioGroupGender= (RadioGroup) findViewById(R.id.radioGroup_gender);
        Button btnRegister =(Button) findViewById(R.id.btnRegister);

        /***Usando el LoginButton**/

        //Inicializando el login Button
        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        //loginButton.setPublishPermissions("publish_actions");
      loginButton.setReadPermissions("public_profile","user_birthday","email");

        // Registro del callback
        callbackManager = CallbackManager.Factory.create();
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.i(LOG_TAG,loginResult.getAccessToken().getUserId());
                accessToken=loginResult.getAccessToken();
                fetchUserData();
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException exception) {
            }
        });


        /***Usando una customView**/
        Button mBtnFacebookLogin = (Button) findViewById(R.id.btnFacebookLogin);
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.i(LOG_TAG, loginResult.getAccessToken().getUserId());
                        accessToken=loginResult.getAccessToken();
                        fetchUserData();
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });

        mBtnFacebookLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LoginManager.getInstance().logInWithPublishPermissions(MainActivity.this, Arrays.asList("publish_actions"));
                LoginManager.getInstance().logInWithReadPermissions(MainActivity.this, Arrays.asList("public_profile", "email", "user_birthday"));

            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                startActivity(intent);

            }
        });


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //necesario para las dos formas de login
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    private void fetchUserData() {

        GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        Log.i(LOG_TAG, "response: " + object.toString());
                        parseFacebookUserDataResponse(object);
                        if(demoUser!=null)
                            updateFormView();

                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields","id,name,link,birthday,gender,email");
        request.setParameters(parameters);
        request.executeAsync();

    }

    private void updateFormView() {

        if(demoUser.getName()!=null)
            textName.setText(demoUser.getName());
        if(demoUser.getName()!=null)
            textMail.setText(demoUser.getEmail());
        if(demoUser.getName()!=null)
            textBirthDate.setText(demoUser.getBirthday());

        if(demoUser.getGender()!=null){
            if(demoUser.getGender().equals("male"))
                radioGroupGender.check(R.id.radio_male);
            else
                radioGroupGender.check(R.id.radio_female);
        }

    }

    private void parseFacebookUserDataResponse(JSONObject object) {

        try {

            demoUser = new User();

            if(!object.isNull("id"))
                demoUser.setId(object.getString("id"));
            if(!object.isNull("name"))
                demoUser.setName(object.getString("name"));
            if(!object.isNull("birthday"))
                demoUser.setBirthday(object.getString("birthday"));
            if(!object.isNull("gender"))
                demoUser.setGender(object.getString("gender"));
            if(!object.isNull("email"))
                demoUser.setEmail(object.getString("email"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
