package com.gigigo.fb.test;

import android.app.Application;

import com.facebook.FacebookSdk;

/**
 * Created by tico on 28/07/15.
 */
public class GigigoTest extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        //Inicializamos el SDK de facebook antes que cualquier otra cosa en la aplicación
        FacebookSdk.sdkInitialize(getApplicationContext());

    }
}
