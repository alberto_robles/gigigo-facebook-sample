package com.gigigo.fb.test;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.ShareApi;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

/**
 * Created by tico on 28/07/15.
 */
public class SecondActivity extends AppCompatActivity {

    private static final String LOG_TAG="GigigoFacebookDemo";
    private CallbackManager callbackManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_second);

        callbackManager = CallbackManager.Factory.create();

        Button btnShareLink = (Button) findViewById(R.id.btnShareLink);
        btnShareLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareLinkContent content = new ShareLinkContent.Builder()
                        .setContentUrl(Uri.parse("http://albertorobles.branded.me/"))
                        .setContentTitle("Prueba Facebook Android SDK 4.2")
                        .setImageUrl(Uri.parse("https://media.licdn.com/mpr/mprx/0_PNIlGliTC02Pu0YjGgc1Ld1TCrDnu1lYb6c-PfP33-uPu0jgF99-KvtT-hgKuPr4vg91cK1X35S9mGrZG0sOzEPbn5SnmG2Yb0sP8l7FixpxmvgcbUEtEwkBfM"))
                        .build();
                ShareApi.share(content, null);
                Log.i(LOG_TAG, "Publicado");
                Toast.makeText(SecondActivity.this, "¡Publicado en tu biografía!", Toast.LENGTH_SHORT).show();

            }
        });


        Button btnShareLink2 = (Button) findViewById(R.id.btnShareLink2);
        btnShareLink2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareLinkContent content = new ShareLinkContent.Builder()
                        .setContentUrl(Uri.parse("http://albertorobles.branded.me/"))
                        .setContentTitle("Prueba Facebook Android SDK 4.2")
                        .setImageUrl(Uri.parse("https://media.licdn.com/mpr/mprx/0_PNIlGliTC02Pu0YjGgc1Ld1TCrDnu1lYb6c-PfP33-uPu0jgF99-KvtT-hgKuPr4vg91cK1X35S9mGrZG0sOzEPbn5SnmG2Yb0sP8l7FixpxmvgcbUEtEwkBfM"))
                        .build();

                if (ShareDialog.canShow(ShareLinkContent.class)) {
                    ShareDialog shareDialog = new ShareDialog(SecondActivity.this);
                    shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {

                        @Override
                        public void onSuccess(Sharer.Result result) {
                            Log.i(LOG_TAG, "Publicado");
                        }

                        @Override
                        public void onCancel() {

                        }

                        @Override
                        public void onError(FacebookException e) {

                        }

                    });

                    shareDialog.show(content);
                    Toast.makeText(SecondActivity.this, "Publicando...", Toast.LENGTH_SHORT).show();

                }
            }
        });
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }



}




/*Bitmap image = ...
SharePhoto photo = new SharePhoto.Builder()
        .setBitmap(image)
        .build();
SharePhotoContent content = new SharePhotoContent.Builder()
        .addPhoto(photo)
        .build();
**/